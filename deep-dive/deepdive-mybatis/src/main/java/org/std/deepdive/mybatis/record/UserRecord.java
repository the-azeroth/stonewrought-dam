package org.std.deepdive.mybatis.record;

public record UserRecord(Long userId, String userName, String transactionId) {

}
