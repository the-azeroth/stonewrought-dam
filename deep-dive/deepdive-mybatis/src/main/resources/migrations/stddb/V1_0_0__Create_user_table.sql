CREATE SCHEMA IF NOT EXISTS std;

CREATE TABLE std."user"
(
    user_id bigserial NOT NULL,
    user_name text NOT NULL,
    transaction_id varchar(64) NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (user_id)
);